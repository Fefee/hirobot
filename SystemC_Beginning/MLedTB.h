#ifndef _MLED_TB_H
#define _MLED_TB_H 1

#include "systemc"
using namespace sc_core;
using namespace sc_dt;
using namespace std;

#define SIZE 8

#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

struct MLedTB: sc_module
{
  tlm_utils::simple_initiator_socket<MLedTB> socket;

  SC_CTOR(MLedTB);

  void thread_process();

};

#endif
