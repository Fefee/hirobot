#ifndef _TB_BUTTON_N_LED_H
#define _TB_BUTTON_N_LED_H

//#include "MLedTB.h"
#include "MLed.h"
#include "MButton.h"

struct TB_Button_n_led: sc_module
{
  //MLedTB *myLedTB;
  MLed    *myLed;
  MButton *myButton;

  SC_CTOR(TB_Button_n_led);

  void buttonAction();
};

#endif
