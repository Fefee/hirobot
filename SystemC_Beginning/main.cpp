#include "MMotorTB.h"
#include "MMotor.h"

int sc_main(int argc, char** ){

  MMotor * myMotor;
  MMotorTB *myMotorTB;
  myMotor = new MMotor("myMotor");
  myMotorTB    = new MMotorTB("myMotorTB");

  myMotor->sMotorPwm.bind( myMotorTB->sMotorTBPwm );
  myMotor->sMotorEnable.bind( myMotorTB->sMotorTBEnable );
  myMotor->sMotorDirection.bind( myMotorTB->sMotorTBDirection );

  myMotorTB->sendEnable();
  myMotorTB->sendDirection();
  myMotorTB->sendPwm();

  return 0;

}
