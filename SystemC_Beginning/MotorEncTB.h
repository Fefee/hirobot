#ifndef _MotorEnc_TB_H
#define _MotorEnc_TB_H 1

#include "Common.h"

#include "systemc"
using namespace sc_core;
using namespace sc_dt;
using namespace std;

#define SIZE 8

#define FORWARD 1
#define BACKWARD 0

#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

struct MotorEncTB : sc_module
{
  tlm_utils::simple_initiator_socket<MotorEncTB> sMotorTBPwm;
  tlm_utils::simple_initiator_socket<MotorEncTB> sMotorTBEnable;
  tlm_utils::simple_initiator_socket<MotorEncTB> sMotorTBDirection;

  tlm_utils::simple_target_socket<MotorEncTB> sQuadDirection;
  tlm_utils::simple_target_socket<MotorEncTB> sQuadSpeed;

  SC_CTOR(MotorEncTB);

  void sendPwm(); // in reality just send dutycycle
  void sendEnable();
  void sendDirection();

  void returnStateTB();

  bool checkDutyCycle();

  virtual void quadDirectionTransport(tlm::tlm_generic_payload& trans, sc_time& delay);

  virtual void quadSpeedTransport(tlm::tlm_generic_payload& trans, sc_time& delay);

  uint8_t mEnable;
  uint8_t mDirection;
  uint8_t mDutyCycle;

  sc_time delay;

  uint8_t mDirectionReceived;
  uint16_t mSpeedReceived;

  string mDirectionStr;

};

#endif
