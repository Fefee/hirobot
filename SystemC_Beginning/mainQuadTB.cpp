//#include "MLedTB.h"
//#include "MLed.h"
#include "MQuadEncoder.h"
#include "MQuadEncoderTB.h"
// #include "mainQuadTB.h"
//#include "Top.h"
//#include "TB_Button_n_Led.h"

int sc_main(int argc, char** ){
//  Top Top("Top");
  std::cout << "************SC_MAIN***********" << '\n';

//Déclaration de classes
  // Classe     Instance de la classe

  MQuadEncoder *myMQuadEncoder;
  MQuadEncoderTB *myMQuadEncoderTB;

  myMQuadEncoder = new MQuadEncoder("MQuadEncoder");
  myMQuadEncoderTB = new MQuadEncoderTB("MQuadEncoderTB");

//lier les sockets
  myMQuadEncoder->sQuadDirection.bind( myMQuadEncoderTB->sQuadDirection );
  myMQuadEncoder->sQuadVitesse.bind( myMQuadEncoderTB->sQuadVitesse);


//Appel des methodes des classes
  myMQuadEncoder->sortieMQuadDirection(true);
  myMQuadEncoder->sortieMQuadVitesse(40);



  return 0;


}
