#define SC_INCLUDE_DYNAMIC_PROCESSES
#include "TB_Button_n_Led.h"
#include <iostream>

TB_Button_n_led::TB_Button_n_led(sc_module_name name) : sc_module(name)
{
    myLed = new MLed("MLed");

    myButton = new MButton("MButton");

    myButton->socket.bind( myLed->socket );
}

void TB_Button_n_led::buttonAction(){
  bool result;
  std::cout << "This is time for the test :" << '\n';
  std::cout << "What is the button value ? 1 or 0 " << '\n';
  std::cin >> result;
  myButton->actOnButton(result);
}
