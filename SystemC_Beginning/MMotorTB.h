#ifndef _MMOTOR_TB_H
#define _MMOTOR_TB_H 1

#include "Common.h"

#include "systemc"
using namespace sc_core;
using namespace sc_dt;
using namespace std;

#define SIZE 8

#define FORWARD 1
#define BACKWARD 0

#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

struct MMotorTB: sc_module
{
  tlm_utils::simple_initiator_socket<MMotorTB> sMotorTBPwm;
  tlm_utils::simple_initiator_socket<MMotorTB> sMotorTBEnable;
  tlm_utils::simple_initiator_socket<MMotorTB> sMotorTBDirection;


  SC_CTOR(MMotorTB);

  void sendPwm(); // in reality just send dutycycle
  void sendEnable();
  void sendDirection();

  bool checkDutyCycle();

  uint8_t mEnable;
  uint8_t mDirection;
  uint8_t mDutyCycle;

  sc_time delay;

};

#endif
