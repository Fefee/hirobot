#include "systemc"
using namespace sc_core;
using namespace sc_dt;
using namespace std;

#define SIZE 8

#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

struct MQuadEncoderTB: sc_module
{
  public :

    tlm_utils::simple_target_socket<MQuadEncoderTB> sQuadDirection;
    tlm_utils::simple_target_socket<MQuadEncoderTB> sQuadVitesse;

    SC_CTOR(MQuadEncoderTB);

    void returnStateTB();

    //initiator
    //void thread_process();

    //target
    virtual void MQuadDirection_transport( tlm::tlm_generic_payload& trans, sc_time& delay );
    virtual void MQuadVitesse_transport( tlm::tlm_generic_payload& trans, sc_time& delay );

  private :

    bool mDirection;
    double mVitesse; // nombre sur 32bit avec une virgule
};
