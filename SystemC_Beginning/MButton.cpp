#include "MButton.h"


MButton::MButton(sc_module_name name) : sc_module(name), socket("socket"){
    mState=false;
}

void MButton::actOnButton(bool state){
  if(mState != state){
    mState = state;
    std::cout << "Passing in actButton with value " << mState << '\n';
    unsigned int data;
    sc_time delay = sc_time(10, SC_NS);
    tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;
    tlm::tlm_command cmd = tlm::TLM_WRITE_COMMAND;

    if (cmd == tlm::TLM_WRITE_COMMAND){
      if (mState == false){
        std::cout << "=======================FALSE================" << '\n';
       data = 0x30;
     }
     else if (mState == true){
       std::cout << "==========================TRUE=====================" << '\n';
       data = 0x31;
     }
     else{
       std::cout << "========================ERROR======================" << '\n';
    }
     trans->set_command( cmd );
     trans->set_address( 0 );
     trans->set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
     trans->set_data_length( 4 );
     trans->set_streaming_width( 4 );
     trans->set_byte_enable_ptr( 0 );
     trans->set_dmi_allowed( false );
     trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

     cout << "On Button side : trans = { " << (cmd ? 'W' : 'R') << ", " << hex << 0
           << " } , data = 0x" << hex << data << " at time " << sc_time_stamp() << endl;

     socket->b_transport( *trans, delay );

     if (trans->is_response_error() )
       SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
    }
    else{
      std::cout << "NOT A TLM_WRITE_COMMAND" << '\n';
    }
  }
  else{
    std::cout << "Button already in this state dude ! Nothing to do" << '\n';
  }

}
