#define SC_INCLUDE_DYNAMIC_PROCESSES
#include "Top.h"

Top::Top(sc_module_name name) : sc_module(name)
{
    myLed = new MLed("MLed");
    //myLedTB    = new MLedTB   ("MLedTB");
    myButton = new MButton("MButton");

    myButton->socket.bind( myLed->socket );
}
