#include "MLedTB.h"


MLedTB::MLedTB(sc_module_name name) : sc_module(name), socket("socket"){

  SC_THREAD(thread_process);
}

void MLedTB::thread_process()
{
  unsigned int data;
  tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;
  sc_time delay = sc_time(10, SC_NS);

  // that part is forced, but it will be used
  tlm::tlm_command cmd = static_cast<tlm::tlm_command>(rand() % 2);
  if (cmd == tlm::TLM_WRITE_COMMAND){
     data = 0x31;
  }
  trans->set_command( cmd );
  trans->set_address( 0 );
  trans->set_data_ptr( reinterpret_cast<unsigned char*>(&data) );
  trans->set_data_length( 4 );
  trans->set_streaming_width( 4 );
  trans->set_byte_enable_ptr( 0 );
  trans->set_dmi_allowed( false );
  trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

  cout << "On TB side : trans = { " << (cmd ? 'W' : 'R') << ", " << hex << 0
        << " } , data = 0x" << hex << data << " at time " << sc_time_stamp() << endl;

  socket->b_transport( *trans, delay );

  if (trans->is_response_error() )
    SC_REPORT_ERROR("TLM-2", "Response error from b_transport");

}
