#include "MLed.h"
#include <iostream>

MLed::MLed(sc_module_name name) : sc_module(name), socket("socket")
{
  socket.register_b_transport(this, &MLed::b_transport);
  for (int i=0; i<SIZE; i++)
    mem[i]='0';
  mValue = false;
}

void MLed::changeToOn(){
  mValue = true;
}

void MLed::changeToOff(){
  mValue = false;
}

void MLed::returnState(){
  if (mValue == true){
    std::cout << "LED ALLUME" << std::endl;
  }
  else{
    std::cout << "LED ETEINTE" << std::endl;
  }
}

void MLed::b_transport( tlm::tlm_generic_payload& trans, sc_time& delay )
{
  this->returnState();
  tlm::tlm_command cmd = trans.get_command();
  sc_dt::uint64    adr = trans.get_address();
  unsigned char*   ptr = trans.get_data_ptr();
  unsigned int     len = trans.get_data_length();
  unsigned char*   byt = trans.get_byte_enable_ptr();
  unsigned int     wid = trans.get_streaming_width();

  cout << "On MLed side : trans = { " << (cmd ? 'W' : 'R') << ", " << hex << 0
        << " } , data = " << hex<< ptr << " at time " << sc_time_stamp() << endl;

  if (strcmp(ptr,"1") == 0){
    std::cout << "Bogoton" << std::endl;
    this->changeToOn();
  }
  else if(strcmp(ptr,"0") == 0){
    std::cout << "Bogotoff"<< std::endl;
    this->changeToOff();
  }
  else{
    std::cout << "Huge failure" << '\n';
  }
  this->returnState();

  if (adr >= sc_dt::uint64(SIZE) || byt != 0 || len > 4 || wid < len)
  SC_REPORT_ERROR("TLM-2",
              "Target does not support given generic payload transaction");

  if ( cmd == tlm::TLM_READ_COMMAND ){
    memcpy(ptr, &mem[adr], len);
  }
  else if ( cmd == tlm::TLM_WRITE_COMMAND )
    memcpy(&mem[adr], ptr, len);

  trans.set_response_status( tlm::TLM_OK_RESPONSE );

}
