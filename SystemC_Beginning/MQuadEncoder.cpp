#include "MQuadEncoder.h"
#include "Common.h"

MQuadEncoder::MQuadEncoder(sc_module_name name) : sc_module(name), sQuadDirection("sQuadDirection"), sQuadSpeed("sQuadSpeed")

{
  sMotorToQuadSpeed.register_b_transport(this, &MQuadEncoder::speedTransport);
  sMotorToQuadDirection.register_b_transport(this, &MQuadEncoder::directionTransport);

  // mSpeed = 0;
  // mDirection = false;
   mWorkingMode = DEFAULT;
   delay = sc_time(10, SC_NS);

}


//potentiometre
void simulateData(){

}


void MQuadEncoder::sortieMQuadDirection(){

    tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;

    tlm::tlm_command cmd = tlm::TLM_WRITE_COMMAND;

    trans->set_command( cmd );
    trans->set_address( 0 );
    trans->set_data_ptr( reinterpret_cast<unsigned char*>(&mDirection) );
    trans->set_data_length( 4 );
    trans->set_streaming_width( 4 );
    trans->set_byte_enable_ptr( 0 );
    trans->set_dmi_allowed( false );
    trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

    sQuadDirection->b_transport( *trans, delay );

    if (trans->is_response_error() )
      SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
}

void MQuadEncoder::sortieMQuadVitesse(){

    tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;

    trans->set_command(tlm::TLM_WRITE_COMMAND);
    trans->set_address( 0 );

    trans->set_data_ptr( reinterpret_cast<unsigned char*>(mSpeed) );
    //trans->set_data_ptr( reinterpret_cast<unsigned char*>(&mSpeed) );

    trans->set_data_length( 4 );
    trans->set_streaming_width( 4 );
    trans->set_byte_enable_ptr( 0 );
    trans->set_dmi_allowed( false );
    trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

    sQuadSpeed->b_transport( *trans, delay );

    if (trans->is_response_error() )
      SC_REPORT_ERROR("TLM-2", "Response error from b_transport");

}

void MQuadEncoder::directionTransport(tlm::tlm_generic_payload& trans, sc_time& delay){
  if (mWorkingMode == DEFAULT) {
    tlm::tlm_command cmd = trans.get_command();
    unsigned char*   ptr = trans.get_data_ptr();
    unsigned int     len = trans.get_data_length();
    sc_dt::uint64	   adr = trans.get_address();
    unsigned char*	 byt = trans.get_byte_enable_ptr();
    unsigned int 	   wid = trans.get_streaming_width();

    if (len > 4) {
        trans.set_response_status( tlm::TLM_BURST_ERROR_RESPONSE );
        return;
    }

    if ( cmd == tlm::TLM_WRITE_COMMAND )
    {
      mDirection = reinterpret_cast<uint8_t>(ptr);
    }
   //std::cout << "Motor going at " << mDutyCycle <<  "% of its max speed \n";
   trans.set_response_status( tlm::TLM_OK_RESPONSE );

   /* code */
  }
  this->sortieMQuadDirection();

}

void MQuadEncoder::speedTransport(tlm::tlm_generic_payload& trans, sc_time& delay){
  if (mWorkingMode == DEFAULT) {
    tlm::tlm_command cmd = trans.get_command();
    unsigned char*   ptr = trans.get_data_ptr();
    unsigned int     len = trans.get_data_length();
    sc_dt::uint64	   adr = trans.get_address();
    unsigned char*	 byt = trans.get_byte_enable_ptr();
    unsigned int 	   wid = trans.get_streaming_width();

    if (len > 4) {
        trans.set_response_status( tlm::TLM_BURST_ERROR_RESPONSE );
        return;
    }

    if ( cmd == tlm::TLM_WRITE_COMMAND )
    {
      mSpeed = reinterpret_cast<uint16_t>(ptr);
    }
   //std::cout << "Motor going at " << mDutyCycle <<  "% of its max speed \n";
   trans.set_response_status( tlm::TLM_OK_RESPONSE );
  }
 this->sortieMQuadVitesse();
}
