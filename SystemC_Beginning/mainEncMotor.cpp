
#include "MMotor.h"
#include "MQuadEncoder.h"
#include "MotorEncTB.h"

int sc_main(int argc, char** ){

  MQuadEncoder *myQuadEncoder;
  MMotor *myMotor;
  MotorEncTB *myTB;

  myQuadEncoder = new MQuadEncoder("MQuadEncoder");
  myMotor= new MMotor("MMotor");
  myTB = new MotorEncTB("MotorEncTB");

//lier les sockets
  myMotor->sMotorToQuadDirection.bind(myQuadEncoder->sMotorToQuadDirection);
  myMotor->sMotorToQuadSpeed.bind(myQuadEncoder->sMotorToQuadSpeed);

  myTB->sMotorTBPwm.bind( myMotor->sMotorPwm );
  myTB->sMotorTBEnable.bind( myMotor->sMotorEnable );
  myTB->sMotorTBDirection.bind( myMotor->sMotorDirection );

  myQuadEncoder->sQuadDirection.bind(myTB->sQuadDirection);
  myQuadEncoder->sQuadSpeed.bind(myTB->sQuadSpeed);

//Appel des methodes des classes

  myTB->sendEnable();
  myTB->sendDirection();
  myTB->sendPwm();



  return 0;


}
