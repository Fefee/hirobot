//#define SC_INCLUDE_DYNAMIC_PROCESSES

#include "systemc"
using namespace sc_core;
using namespace sc_dt;
using namespace std;


#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

#define SIZE 8

struct MQuadEncoder: sc_module
{
  public :

    tlm_utils::simple_initiator_socket<MQuadEncoder> sQuadDirection;
    tlm_utils::simple_initiator_socket<MQuadEncoder> sQuadSpeed;

    tlm_utils::simple_target_socket<MQuadEncoder> sMotorToQuadSpeed;
    tlm_utils::simple_target_socket<MQuadEncoder> sMotorToQuadDirection;

    SC_CTOR(MQuadEncoder);

    //utiliser les lib de sa platforme pour afficher un potentiometre
    void simulateData();


    void sortieMQuadDirection();

    void sortieMQuadVitesse();

    virtual void directionTransport(tlm::tlm_generic_payload& trans, sc_time& delay);

    virtual void speedTransport(tlm::tlm_generic_payload& trans, sc_time& delay);

  private :

    uint8_t mDirection;
    uint8_t mWorkingMode;

    uint16_t mSpeed; // nombre sur 32bit avec une virgule

    sc_time delay;
};
