#include "MMotorTB.h"

// Sending Direction ok.


MMotorTB::MMotorTB(sc_module_name name) : sc_module(name), sMotorTBPwm("sMotorTBPwm")\
, sMotorTBEnable("sMotorTBEnable"), sMotorTBDirection("sMotorTBDirection"){

  delay = sc_time(10, SC_NS);
  //  mPwm.data[]
  mEnable = true;
  mDirection = FORWARD;
  mDutyCycle = 100;
}

void MMotorTB::sendPwm(){
  // TO COMPLETE, NOT FUNCTIONAL

  if (this->checkDutyCycle()) {
    tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;
    trans->set_command( tlm::TLM_WRITE_COMMAND );
    trans->set_address( 0 );
    trans->set_data_ptr(reinterpret_cast<unsigned char*>(mDutyCycle));
    trans->set_data_length( 1 );
    trans->set_streaming_width( 3 );
    trans->set_byte_enable_ptr( 0 );
    trans->set_dmi_allowed( false );
    trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

    sMotorTBPwm->b_transport( *trans, delay );
    //sMotorTBEnable->b_transport( *trans2, delay );
    //sMotorTBDirection->b_transport( *trans3, delay );

    if (trans->is_response_error() )
       SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
  }
  else
    std::cout << "Could not send a dutycycle like yours" << std::endl;

}

void MMotorTB::sendEnable(){
  tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;

  trans->set_command( tlm::TLM_WRITE_COMMAND );
  trans->set_address( 0 );
  trans->set_data_ptr( reinterpret_cast<unsigned char *>(mEnable) );
  trans->set_data_length( 1 );
  trans->set_streaming_width(43 );
  trans->set_byte_enable_ptr( 0 );
  trans->set_dmi_allowed( false );
  trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

  sMotorTBEnable->b_transport( *trans, delay );

  if (trans->is_response_error() )
     SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
}


void MMotorTB::sendDirection(){
  tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;

  trans->set_command( tlm::TLM_WRITE_COMMAND );
  trans->set_address( 0 );
  trans->set_data_ptr( reinterpret_cast<unsigned char *>(mDirection) );
  trans->set_data_length( 1 );
  trans->set_streaming_width(43 );
  trans->set_byte_enable_ptr( 0 );
  trans->set_dmi_allowed( false );
  trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

  sMotorTBDirection->b_transport( *trans, delay );

  if (trans->is_response_error() )
     SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
}

bool MMotorTB::checkDutyCycle(){
  if (mDutyCycle > 100 || mDutyCycle < 0)
    return false;
  return true;
}

  //cout << "On TB side : trans = { " << (cmd ? 'W' : 'R') << ", " << hex << 0
  //      << " } , data = " << data << " at time " << sc_time_stamp() << endl;
//
//   tlm::tlm_generic_payload* trans2 = new tlm::tlm_generic_payload;
//   // that part is forced, but it will be used
//   uint8_t *data2 = new uint8_t[1];
//   data2[0]=0x31;
//   trans2->set_command( cmd );
//   trans2->set_address( 1 );
//   trans2->set_data_ptr( reinterpret_cast<unsigned char*>(data2) );
//   trans2->set_data_length( 1 );
//   trans2->set_streaming_width( 4 );
//   trans2->set_byte_enable_ptr( 0 );
//   trans2->set_dmi_allowed( false );
//   trans2->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
//
//   //cout << "On TB side : trans = { " << (cmd ? 'W' : 'R') << ", " << hex << 1
//   //      << " } , data = " << data2 << " at time " << sc_time_stamp() << endl;
//
// tlm::tlm_generic_payload* trans3 = new tlm::tlm_generic_payload;
//   // that part is forced, but it will be used
//   uint8_t *data3 = new uint8_t[1];
//   data3[0]=0x31;
//   trans3->set_command( cmd );
//   trans3->set_address( 2 );
//   trans3->set_data_ptr( reinterpret_cast<unsigned char*>(data3) );
//   trans3->set_data_length( 1 );
//   trans3->set_streaming_width( 4 );
//   trans3->set_byte_enable_ptr( 0 );
//   trans3->set_dmi_allowed( false );
//   trans3->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );
//
//   //cout << "On TB side : trans = { " << (cmd ? 'W' : 'R') << ", " << hex << 2
//   //      << " } , data = " << data3 << " at time " << sc_time_stamp() << endl;


//}
