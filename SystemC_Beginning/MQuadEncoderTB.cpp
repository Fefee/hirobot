#include "MQuadEncoderTB.h"


MQuadEncoderTB::MQuadEncoderTB(sc_module_name name) : sc_module(name), sQuadDirection("sQuadDirection"), sQuadVitesse("sQuadVitesse")
{
  std::cout << "***********MQuadEncoderTB************" << '\n';
  //initiator
  //SC_THREAD(thread_process);

  //target
  sQuadDirection.register_b_transport(this, &MQuadEncoderTB::MQuadDirection_transport);
  sQuadVitesse.register_b_transport(this, &MQuadEncoderTB::MQuadVitesse_transport);

  mDirection=false;

}


//////////////
///fonction///


//afficher dans le terminal
void MQuadEncoderTB::returnStateTB(){
  std::cout << "   MQuadEncoderTB::returnStateTB()" << '\n';
  //interrupteur fonctionnement via entrée / action interne
  /*if (mFonctionnement == true){
    std::cout << "mFonctionnement Via Entrée" << std::endl;
  }
  else{
    std::cout << "mFonctionnement Via Action Interne" << std::endl;
  }*/

  //interrupteur direction avant / arriere //*******ne sert pas ici !!!
  if (mDirection == true){
    std::cout << "mDirection Avant" << std::endl;
  }
  else{
    std::cout << "mDirection Arriere" << std::endl;
  }

  //potentiometre
  //std::cout << "mVitesse ="<< mVitesse<<" tr/min" << std::endl; //****** rajouter la vitesse dans MQuad !!!!! l'envoyer !!!
}

//*****************************************************************************
//target DIRECTION
void MQuadEncoderTB::MQuadDirection_transport( tlm::tlm_generic_payload& trans, sc_time& delay )
{
  std::cout << '\n';
  std::cout << "***SOCKET TARGET : sQuadDirection" << '\n';
  std::cout << "   MQuadEncoderTB::b_transport" << '\n';
  //this->returnStateTB();
  tlm::tlm_command cmd = trans.get_command();
  sc_dt::uint64    adr = trans.get_address();
  unsigned char*   ptr = trans.get_data_ptr();
  unsigned int     len = trans.get_data_length();
  unsigned char*   byt = trans.get_byte_enable_ptr();
  unsigned int     wid = trans.get_streaming_width();

  cout << "On MQuadEncoderTB side : trans = { " << (cmd ? 'W' : 'R') << ", " << hex << 0
        << " } , data = " << hex<< ptr << " at time " << sc_time_stamp() << endl;

  if (strcmp(ptr,"1") == 0){
    std::cout << "MQuadEncoderTB direction = 1" << std::endl;
  }
  else if(strcmp(ptr,"0") == 0){
    std::cout << "MQuadEncoderTB direction = 0"<< std::endl;
  }
  else{
    std::cout << "Huge failure" << '\n';
  }

  //this->returnStateTB();

  if (adr >= sc_dt::uint64(SIZE) || byt != 0 || len > 4 || wid < len)
  SC_REPORT_ERROR("TLM-2",
              "Target does not support given generic payload transaction");

  /*if ( cmd == tlm::TLM_READ_COMMAND ){
    memcpy(ptr, &mem[adr], len);
  }
  else if ( cmd == tlm::TLM_WRITE_COMMAND )
    memcpy(&mem[adr], ptr, len);
    */

  trans.set_response_status( tlm::TLM_OK_RESPONSE );

}


//*****************************************************************************
//target VITESSE
void MQuadEncoderTB::MQuadVitesse_transport( tlm::tlm_generic_payload& trans, sc_time& delay )
{
  std::cout << '\n';
  std::cout << "***SOCKET TARGET : sQuadVitesse" << '\n';
  std::cout << "   MQuadEncoderTB::MQuadVitesse_transport" << '\n';
  //this->returnStateTB();
  tlm::tlm_command cmd = trans.get_command();
  sc_dt::uint64    adr = trans.get_address();
  unsigned char*   ptr = trans.get_data_ptr();
  unsigned int     len = trans.get_data_length();
  unsigned char*   byt = trans.get_byte_enable_ptr();
  unsigned int     wid = trans.get_streaming_width();

  cout << "On MQuadEncoderTB side : trans = { " << (cmd ? 'W' : 'R') << ", " << hex << 0
       << " } , data = " << hex<< ptr << " at time " << sc_time_stamp() << endl;

  std::cout << "MQuadEncoderTB vitesse =" << hex<< ptr << '\n';
  std::cout << "MQuadEncoderTB vitesse =" << reinterpret_cast<uint8_t>(ptr) << '\n';

//#############################################################################
  char a = '\(';
  std::cout << "a =" << (int)a << '\n';
//#############################################################################

  if (adr >= sc_dt::uint64(SIZE) || byt != 0 || len > 4 || wid < len)
  SC_REPORT_ERROR("TLM-2",
              "Target does not support given generic payload transaction");

  /*if ( cmd == tlm::TLM_READ_COMMAND ){
    memcpy(ptr, &mem[adr], len);
  }
  else if ( cmd == tlm::TLM_WRITE_COMMAND )
    memcpy(&mem[adr], ptr, len);
    */

  trans.set_response_status( tlm::TLM_OK_RESPONSE );

}
