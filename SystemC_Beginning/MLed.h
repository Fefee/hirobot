#ifndef _MLED_H
#define _MLED_H 1

#define SC_INCLUDE_DYNAMIC_PROCESSES

#include "systemc"
using namespace sc_core;
using namespace sc_dt;
using namespace std;


#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

#define SIZE 8

struct MLed: sc_module
{
  public :

    tlm_utils::simple_target_socket<MLed> socket;

    SC_CTOR(MLed);

    void changeToOn();

    void changeToOff();

    void returnState();

    virtual void b_transport( tlm::tlm_generic_payload& trans, sc_time& delay );

  private :

    unsigned char mem[SIZE];

    bool mValue;

};

#endif
