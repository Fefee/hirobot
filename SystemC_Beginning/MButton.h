#ifndef _MBUTTON_H
#define _MBUTTON_H 1

#define SC_INCLUDE_DYNAMIC_PROCESSES

#include "systemc"
using namespace sc_core;
using namespace sc_dt;
using namespace std;


#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

struct MButton: sc_module
{
  public :

    tlm_utils::simple_initiator_socket<MButton> socket;

    SC_CTOR(MButton);

    void actOnButton(bool state);

    bool getState();

  private :

    bool mState;

    int mId;
};

#endif
