#ifndef _MMOTOR_H
#define _MMOTOR_H 1

#define SC_INCLUDE_DYNAMIC_PROCESSES

#define MAX_SPEED 4000

#define LOWEST_SPEED 0

#include "Common.h"
#include "systemc"
using namespace sc_core;
using namespace sc_dt;
using namespace std;


#include "tlm.h"
#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

struct MMotor: sc_module
{
  public :

    tlm_utils::simple_target_socket<MMotor> sMotorPwm;
    tlm_utils::simple_target_socket<MMotor> sMotorEnable;
    tlm_utils::simple_target_socket<MMotor> sMotorDirection;

    tlm_utils::simple_initiator_socket<MMotor> sMotorToQuadSpeed;
    tlm_utils::simple_initiator_socket<MMotor> sMotorToQuadDirection;

    SC_CTOR(MMotor);

    void threadDisplayProcess(); //will only display, no return necessary

    virtual void enableTransport(tlm::tlm_generic_payload& trans, sc_time& delay);

    virtual void directionTransport(tlm::tlm_generic_payload& trans, sc_time& delay);

    virtual void pwmTransport(tlm::tlm_generic_payload& trans, sc_time& delay);

    void sendDirection();

    void sendSpeed();

    void computeSpeed();

  private :

    uint16_t mSpeed;
    uint8_t mDirection; // true = right, false = left
    uint8_t mEnable; // no job if no enable
    uint8_t mDutyCycle;
    uint8_t mPeriod;

    string mDirectionStr;
    sc_time  delay;
    uint8_t toQuadEnc;
};

#endif
