#include "MMotor.h"

// TROIS SOCKETS EN ENTREE

// exemple pwm, envoi période, dutycycle, juste au changement

MMotor::MMotor(sc_module_name name) : sc_module(name), sMotorPwm("sMotorPwm")\
, sMotorEnable("sMotorEnable"), sMotorDirection("sMotorDirection"){

    sMotorPwm.register_b_transport(this, &MMotor::pwmTransport);
    sMotorEnable.register_b_transport(this, &MMotor::enableTransport);
    sMotorDirection.register_b_transport(this, &MMotor::directionTransport);

    SC_THREAD(threadDisplayProcess);

    delay=sc_time(10, SC_NS);

    toQuadEnc = true;
}

void MMotor::threadDisplayProcess(){
    //while (true){
    //  std::cout << "Motor is going at " << mSpeed << " on " << mRotationDir << '\n';
    //  wait(5);
    //}
}

void MMotor::sendDirection(){
  tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;
  trans->set_command( tlm::TLM_WRITE_COMMAND );
  trans->set_address( 0 );
  trans->set_data_ptr(reinterpret_cast<unsigned char*>(mDirection));
  trans->set_data_length( 1 );
  trans->set_streaming_width( 3 );
  trans->set_byte_enable_ptr( 0 );
  trans->set_dmi_allowed( false );
  trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

  sMotorToQuadDirection->b_transport(*trans, delay);

  if (trans->is_response_error() )
     SC_REPORT_ERROR("TLM-2", "Response error from b_transport");

}

void MMotor::sendSpeed(){

  tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;
  trans->set_command( tlm::TLM_WRITE_COMMAND );
  trans->set_address( 0 );
  trans->set_data_ptr(reinterpret_cast<unsigned char*>(mSpeed));
  trans->set_data_length( 1 );
  trans->set_streaming_width( 3 );
  trans->set_byte_enable_ptr( 0 );
  trans->set_dmi_allowed( false );
  trans->set_response_status( tlm::TLM_INCOMPLETE_RESPONSE );

  sMotorToQuadSpeed->b_transport(*trans, delay);

  if (trans->is_response_error() )
     SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
}


void MMotor::pwmTransport(tlm::tlm_generic_payload& trans, sc_time& delay){
  tlm::tlm_command cmd = trans.get_command();
  unsigned char*   ptr = trans.get_data_ptr();
  unsigned int     len = trans.get_data_length();
  sc_dt::uint64	   adr = trans.get_address();
  unsigned char*	 byt = trans.get_byte_enable_ptr();
  unsigned int 	   wid = trans.get_streaming_width();

  if (len > 4) {
      trans.set_response_status( tlm::TLM_BURST_ERROR_RESPONSE );
      return;
  }

  if ( cmd == tlm::TLM_WRITE_COMMAND )
  {
    mDutyCycle = reinterpret_cast<uint8_t>(ptr);
  }

 //std::cout << "Motor going at " << mDutyCycle <<  "% of its max speed \n";
 trans.set_response_status( tlm::TLM_OK_RESPONSE );
 this->computeSpeed();
}

void MMotor::enableTransport(tlm::tlm_generic_payload& trans, sc_time& delay){
  tlm::tlm_command cmd = trans.get_command();
  unsigned char*   ptr = trans.get_data_ptr();
  unsigned int     len = trans.get_data_length();
  sc_dt::uint64	   adr = trans.get_address();
  unsigned char*	 byt = trans.get_byte_enable_ptr();
  unsigned int 	   wid = trans.get_streaming_width();

  mEnable = reinterpret_cast<uint8_t>(ptr);

  trans.set_response_status( tlm::TLM_OK_RESPONSE );

}

void MMotor::directionTransport(tlm::tlm_generic_payload& trans, sc_time& delay){
  tlm::tlm_command cmd = trans.get_command();
  unsigned char*   ptr = trans.get_data_ptr();
  unsigned int     len = trans.get_data_length();
  sc_dt::uint64	   adr = trans.get_address();
  unsigned char*	 byt = trans.get_byte_enable_ptr();
  unsigned int 	   wid = trans.get_streaming_width();

  mDirection = reinterpret_cast<uint8_t>(ptr);

  trans.set_response_status( tlm::TLM_OK_RESPONSE );

  if(mDirection == 1)
      mDirectionStr="Forward";
  else if(mDirection == 0)
      mDirectionStr="Backward";
}

void MMotor::computeSpeed(){
  if (mEnable == true){
    mSpeed = mDutyCycle * (MAX_MOTOR_SPEED / 100);
    std::cout << "Motor going at " << mSpeed << " trs/min on direction " << mDirectionStr << '\n';
    if (toQuadEnc == true) {
      this->sendDirection();
      this->sendSpeed();
    }

  }
}
