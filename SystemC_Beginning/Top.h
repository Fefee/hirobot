#ifndef _TOP_H
#define _TOP_H 1

//#include "MLedTB.h"
#include "MLed.h"
#include "MButton.h"

struct Top: sc_module
{
  //MLedTB *myLedTB;
  MLed    *myLed;
  MButton *myButton;

  SC_CTOR(Top);
};

#endif
