### HIROBOT

Projet Hirobot en colaboration avec la startup [Hiventive][id/name]  dans le cadre du projet numérique de 3eme année de la filière SEE de l'enseirb-matmeca.

[id/name]:https://www.hiventive.com/#page-top  

<a href="https://www.hiventive.com/#page-top" rel="some text">![Foo](https://www.frenchtechbordeaux.com/wp-content/uploads/2019/07/hiventive-horizontal-gradient@2x.png)</a>

## Introduction
Le HiRobot est un robot modulaire capable d'évoluer dans différents environnements. Il est capable de se déplacer dans un environment sec et ensoleillé, dans un environnement humide et froid, … Cependant, comme vous n'aurez pas accès à tout ces environments, le but de ce projet consistera à simuler entièrement le robot.

## Objectif 1 : Découverte de la simulation
Cette première étape consiste à découvrir la plateforme de simulation Hiventive. Pour cela, vous commencerez par effectuer le tutoriel qui permet de lancer une simulation d'une Raspberry Pi 3 afin de vous familiariser avec l'outil. Vous rajouterez ensuite un capteur et une LED  et vous tenterez d’interagir avec ceux-ci à travers les GPIO (en python ou en bash par exemple).

## Objectif 2 : Partie logicielle MCU
Le deuxième étape consiste à initialiser le développement logiciel. Pour cela, vous initialiserez un projet pour le MCU avec la chaine d'outils de votre choix. Il faudra être capable de contrôler LEDs, boutons, moteurs et encodeurs quadratiques. L’information sera remontée au MPU à travers l'UART. Le MCU pourra aussi recevoir des commandes du MPU par l'UART. Un protocole de communication devra ainsi être mis en place pour s'échanger de l'information. Vous rajouterez le MCU à la simulation et vous validerez le bon fonctionnement.

## Objectif 3 : Partie logicielle MPU
Le troisième étape consiste à initialiser le développement logiciel côté MPU. Il faut implémenter le protocole de communication développé dans le MCU côté Raspberry Pi. Connectez le MPU et MCU sur la simulation et validez la bonne communication. Vous devez être capable de contrôler tous les périphériques connectés au MCU depuis le MPU.

## Objectif 4 : Modélisation
Dans cette étape, vous allez modéliser des composants électroniques : le moteur et l'encodeur rotatif. Ajoutez ces composants dans votre simulation et validez le bon fonctionnement.

## Objectif 5 : Simulation complète
Le dernière étape consiste à réaliser le modèle de l’environnement de simulation qui va venir alimenter les capteurs pour la simulation. Pour cela, vous utiliserez le Python pour décrire votre environnement et venir vous connecter au simulateur ainsi qu’aux différents capteurs. Vous pourrez alors simuler entièrement le robot dans son environnement.
C'est aussi le moment de faire le bilan : avantages et inconvénients de la simulation.

## Doc STM32F4
https://www.st.com/content/ccc/resource/technical/document/reference_manual/3d/6d/5a/66/b4/99/40/d4/DM00031020.pdf/files/DM00031020.pdf/jcr:content/translations/en.DM00031020.pdf
