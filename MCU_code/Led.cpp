#include "Led.h"

Led::Led(){
  
}

void Led::init(uint8_t id, uint8_t pin) { 
  mState = LOW;
  mId = id;
  mPin = pin;
  pinMode(pin, OUTPUT);

}

/*
// get Led state
*/
bool Led::getState(){
  return mState;
}

/*
// light_up
*/
void Led::light_up(){
  digitalWrite(mPin, HIGH);
  mState = HIGH;
}
/*
// light_down
*/
void Led::light_down(){
  digitalWrite(mPin, LOW);
  mState = LOW;
}
