#include "Button.h"

Button::Button(){
}

void Button::init(uint8_t id, uint8_t pin, uint8_t mode) {
  mId = id;
  mPin = pin;
  pinMode(pin, mode);
}

/*
// get button state
*/
bool Button::getState(){
  return digitalRead(mPin);
}
