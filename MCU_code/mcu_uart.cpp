#include "mcu_uart.h"

MCU_Uart::MCU_Uart() {}

void MCU_Uart::initDefault() {
  Serial.setRx(PA10);
  Serial.setTx(PA9);
  Serial.begin(115200);
  Serial.flush();
}

void MCU_Uart::init(uint32_t baudrate) {
  Serial.begin(baudrate);
}

uint8_t MCU_Uart::recept(char* frame) {
  if (Serial.available() > 0) {      //While data available on serial port
    int temp;
    char t_recep[50] = {0};

    temp = Serial.read();               //read one byte on serial port
    if (temp == START_OF_TRAME) {       //if this byte is equal to START_OF_TRAME else ignore
      int i = 0;
      unsigned long t = 0;
      int len = 0;

      t_recep[0] = START_OF_TRAME;
      while ((!((t_recep[i] == END_OF_TRAME) && (i>=2))) && t < TIMEOUT) {
        t = millis();
        while (Serial.available() <= 0 && (millis() - t) < TIMEOUT);
        t = millis() - t;

        t_recep[++i] = Serial.read(); //Read one byte
      }

      if (t >= TIMEOUT) { //Check timeout
        sendError(ERROR_TIMEOUT);
        return 0;
      }

      if (verifCHK(t_recep, len)) {
        memcpy(frame, t_recep+1, i-2);
        return i-2;
      }
      else {
        //Error : bad CHK
        sendError(ERROR_BAD_CHK);
        return 0;
      }
    }
  } else
    return 0;
}

// Calculate CHK for serial transmission
unsigned char MCU_Uart::calculCHK(char *trame, uint8_t len)
{
  unsigned char chk = 0;
  for (uint8_t i = 0; i < len; i++)
  {
    chk += trame[i];
  }
  return chk + END_OF_TRAME;
}

// Check CHK after serial reception
int MCU_Uart::verifCHK(char *trame, uint8_t len)
{
  return 1;
  unsigned char chk = 0;
  int i = 0;
  for (i = 0; i < len - 2; i++)
  {
    chk += trame[i];
  }
  if ((chk + END_OF_TRAME) % 256 == (unsigned char)trame[i])
    return 1;
  else
    return 0;
}

void MCU_Uart::sendError(uint8_t id) {
  char error[3];
  error[0] = 'e';
  error[1] = id;
  error[2] = 0;

  sendFrame(error, 2);
}

void MCU_Uart::sendFrame(char* frame, uint8_t lenght) {
  char t_send[100];
  t_send[0] = START_OF_TRAME;
  memcpy (t_send + 1, frame, lenght);
  memset (t_send + lenght + 1, calculCHK(t_send, lenght + 1), 1);
  memset (t_send + lenght + 2, END_OF_TRAME, 1);
  Serial.write(t_send, lenght + 3);
}
