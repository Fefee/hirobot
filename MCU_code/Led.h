#include "arduino.h"
#include "variables.h"

class Led {

  public:

    Led();
    void init(uint8_t id, uint8_t pin);
    bool getState();
    void light_up();
    void light_down();

  private:
    bool mState;
    uint8_t mId;
    uint8_t mPin;
};
