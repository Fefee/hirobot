#include "variables.h"
#include "arduino.h"
class Motor {

  public :

    Motor();
    void init(uint8_t id, uint8_t enablePin, uint8_t directionPin, uint8_t pwmPin);
    void rotate(bool rotationDir, uint8_t speed);
    void stop();

  private :

    uint8_t mId;
    uint8_t mEnablePin;
    uint8_t mDirectionPin;
    uint8_t mPwmPin;

};
