#include "QuadEncoder.h"

QuadEncoder::QuadEncoder() {}

void QuadEncoder::init(uint8_t id, uint8_t pin1, uint8_t pin2, void interruptFunc1(void), void interruptFunc2(void)) {
   mId = id;
   mPin1 = pin1;
   mPin2 = pin2;
   configureGPIO(interruptFunc1, interruptFunc2);
}
void QuadEncoder::getMove(uint16_t *distance, bool *direction) {
    * direction = mDirection;
    * distance = mNbStep/STEP_PER_MM; // conversion step to mm
    mNbStep = 0;
}

void QuadEncoder::incrementStep() {
    mNbStep++;
}

void QuadEncoder::setDirection() {
    if (digitalRead(mPin1) == HIGH)
        mDirection = FORWARD;
    else
        mDirection = BACKWARD;
}

void QuadEncoder::configureGPIO(void interruptFunc1(void), void interruptFunc2(void)) {
    pinMode(mPin1, INPUT);
    pinMode(mPin2, INPUT);

    attachInterrupt(digitalPinToInterrupt(mPin1), interruptFunc1, CHANGE);
    attachInterrupt(digitalPinToInterrupt(mPin2), interruptFunc2, RISING);
}
