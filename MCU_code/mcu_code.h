#include "QuadEncoder.h"
#include "variables.h"
#include "mcu_uart.h"
#include "Led.h"
#include "Button.h"
#include "Motor.h"

QuadEncoder encoder[2];

MCU_Uart mySerial;

Led led[4];

Button button[5];

Motor motor[2];

void encoder1Pin1Event();
void encoder1Pin2Event();
void encoder2Pin1Event();
void encoder2Pin2Event();
