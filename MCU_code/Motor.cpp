#include "Motor.h"



/*
  // Default constructor speed 0, enable 0, rotation LEFT
*/

Motor::Motor() {
}

/*
  // Custom constructor speed 0, enable 0, rotation LEFT, fixed id
*/

void Motor::init(uint8_t id, uint8_t enablePin, uint8_t directionPin, uint8_t pwmPin) {
  mId = id;
  mEnablePin = enablePin;
  mDirectionPin = directionPin;
  mPwmPin = pwmPin;

  pinMode(mEnablePin, OUTPUT);
  pinMode(mDirectionPin, OUTPUT);
  pinMode(mPwmPin, OUTPUT);

  digitalWrite(mEnablePin, LOW);
  digitalWrite(mDirectionPin, LOW);
  digitalWrite(mPwmPin, LOW);
}

/*
  // Funtion rotation launch speed 0, enable 0, rotation LEFT
*/

void Motor::rotate(bool rotationDir, uint8_t speed) {
  digitalWrite(mEnablePin, HIGH);
  digitalWrite(mDirectionPin, rotationDir);
  analogWrite(mPwmPin, speed);
}

void Motor::stop() {
  digitalWrite(mEnablePin, LOW);
  digitalWrite(mPwmPin, LOW);
}
