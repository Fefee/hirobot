//-------------------------------------------------------------
//Motor

#define MOTOR1_PIN_ENABLE     D6
#define MOTOR1_PIN_DIRECTION  D9
#define MOTOR1_PIN_PWM        D10

#define MOTOR2_PIN_ENABLE     PA7
#define MOTOR2_PIN_DIRECTION  D8
#define MOTOR2_PIN_PWM        PB0//D9


#define FORWARD true // used for rotation direction
#define BACKWARD false // same
//-------------------------------------------------------------

//-------------------------------------------------------------
// Quad encoder
#define ENCODER1_PIN1 PA1  //int1
#define ENCODER1_PIN2 D7 //D9 //PB0 //D11  //int0
#define ENCODER2_PIN1 D12  //int4
#define ENCODER2_PIN2 PA3 //int3

#define STEP_PER_MM 4
//-------------------------------------------------------------

//-------------------------------------------------------------
// Button
#define PUSHED true
#define RELEASED false

//-------------------------------------------------------------

//-------------------------------------------------------------
// Uart
#define START_OF_TRAME  0x02
#define END_OF_TRAME    0x03

#define TIMEOUT               100

//Error code
#define ERROR_BAD_CHK         0x00
#define ERROR_BAD_COMMAND     0x01
#define ERROR_TIMEOUT         0x02

#define BUTTON  0x60
#define LED     0x40
#define ENCODER 0x20
#define MOTEUR  0x00

#define RIGHT   0x00
#define LEFT    0x02
#define BOTH    0x04
#define DIS     0x06
//-------------------------------------------------------------


//-------------------------------------------------------------
// Led
#define LED_RIGHT_PIN     PA4
#define LED_LEFT_PIN      PB7
#define LED_BACKWARD_PIN  PA2
//-------------------------------------------------------------

//-------------------------------------------------------------
// Button
#define BUTTON_FORWARD_PIN  D2
#define BUTTON_BACKWARD_PIN D11 //PA2
#define BUTTON_RIGHT_PIN    D5
#define BUTTON_LEFT_PIN     D13

//-------------------------------------------------------------
