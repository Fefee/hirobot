#include "mcu_code.h"

void setup() {
  mySerial.initDefault();

  encoder[0].init('L', ENCODER1_PIN1, ENCODER1_PIN2, encoder1Pin1Event, encoder1Pin2Event);
  encoder[1].init('R', ENCODER2_PIN1, ENCODER2_PIN2, encoder2Pin1Event, encoder2Pin2Event);

  motor[0].init('L', MOTOR1_PIN_ENABLE, MOTOR1_PIN_DIRECTION, MOTOR1_PIN_PWM);
  motor[1].init('R' , MOTOR2_PIN_ENABLE, MOTOR2_PIN_DIRECTION, MOTOR2_PIN_PWM);

  led[0].init(0, LED_RIGHT_PIN);
  led[1].init(1, LED_LEFT_PIN);
  led[2].init(2, LED_BACKWARD_PIN);


  button[0].init(0, BUTTON_FORWARD_PIN, INPUT_PULLUP);
  button[1].init(1, BUTTON_BACKWARD_PIN, INPUT_PULLUP);
  button[2].init(2, BUTTON_RIGHT_PIN, INPUT_PULLUP);
  button[3].init(3, BUTTON_LEFT_PIN, INPUT_PULLUP);
}

void loop() {
  char receptFrame[50];
  uint8_t receptLen = 0;
  char t_send[8];
  receptLen = mySerial.recept(receptFrame);
  if (receptLen > 0) {
    switch (receptFrame[0] & 0xE0) {
      case LED:
        if ((receptFrame[0] & 0x01) == 0)
          led[((receptFrame[0] & 0x1E) >> 1) - 1].light_down();

        else if ((receptFrame[0] & 0x01) == 1)
          led[((receptFrame[0] & 0x1E) >> 1) - 1].light_up();

        break;
      case BUTTON :
        
        t_send[0] = 48 + ((uint8_t)button[((receptFrame[0] & 0x1E) >> 1) - 1].getState());
        mySerial.sendFrame(t_send, 1);

        break;
      case MOTEUR :
        bool rotationDir;
        if (((receptFrame[0] & 0x1E) != DIS) && ((receptFrame[0] & 0x01) == FORWARD))
          rotationDir = FORWARD;
        else
          rotationDir = BACKWARD;

        if ((receptFrame[0] & 0x1E) == LEFT)
           motor[0].rotate(rotationDir, receptFrame[1]);

        else if ((receptFrame[0] & 0x1E) == RIGHT)
          motor[1].rotate(rotationDir, receptFrame[1]);

        else if ((receptFrame[0] & 0x1E) == BOTH) {
          motor[0].rotate(rotationDir, receptFrame[1]);
          motor[1].rotate(rotationDir, receptFrame[1]);
        } else if ((receptFrame[0] & 0x1E) == DIS) {
          motor[0].stop();
          motor[1].stop();

        } else
          mySerial.sendError(ERROR_BAD_COMMAND);

        break;
      case ENCODER :
        uint16_t distance;
        bool direction;

        if ((receptFrame[0] & 0x0E) == LEFT) {
          encoder[0].getMove(&distance, &direction);
          t_send[0] = direction;
          t_send[1] = ((distance & 0xFF00) >> 8);
          t_send[2] = distance & 0x00FF;
          mySerial.sendFrame(t_send, 3);

        } else if ((receptFrame[0] & 0x0E) == RIGHT) {
          encoder[1].getMove(&distance, &direction);
          t_send[0] = direction;
          t_send[1] = ((distance & 0xFF00) >> 8);
          t_send[2] = distance & 0x00FF;
          mySerial.sendFrame(t_send, 3);

        } else if ((receptFrame[0] & 0x0E) == BOTH) {
          encoder[0].getMove(&distance, &direction);
          t_send[0] = direction;
          t_send[1] = ((distance & 0xFF00) >> 8);
          t_send[2] = distance & 0x00FF;
          encoder[1].getMove(&distance, &direction);
          t_send[3] = ((distance & 0xFF00) >> 8);
          t_send[4] = distance & 0x00FF;
          t_send[0] |= (direction & 0x01) <<4;
          mySerial.sendFrame(t_send, 5);

        } else
          mySerial.sendError(ERROR_BAD_COMMAND);
        break;
      default:
        mySerial.sendError(ERROR_BAD_COMMAND);
        break;
    }
  }
}

  void encoder1Pin1Event() {
    encoder[0].incrementStep();
  }

  void encoder1Pin2Event() {
    encoder[0].setDirection();
  }

  void encoder2Pin1Event() {
    encoder[1].incrementStep();
  }

  void encoder2Pin2Event() {
    encoder[1].setDirection();
  }
