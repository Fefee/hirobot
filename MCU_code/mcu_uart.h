#include "arduino.h"
#include "variables.h"

class MCU_Uart{

  public :
    MCU_Uart();
    void initDefault();
    void init(uint32_t baudrate);
    void sendFrame(char* frame, uint8_t lenght);
    uint8_t recept(char* frame);
    void sendError(uint8_t ID);
    
  private :
    int verifCHK(char *trame, uint8_t len);
    unsigned char calculCHK(char *trame, uint8_t len);
};
