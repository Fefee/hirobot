#include "Arduino.h"
#include "variables.h"

class Button {

  public :
    Button();
    void init(uint8_t id, uint8_t pin, uint8_t mode);
    bool getState();

  private :
    uint8_t mId;
    uint8_t mPin;
};
