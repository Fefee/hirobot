#include "arduino.h"
#include "variables.h"


class QuadEncoder {

  public :
    QuadEncoder();
    void init(uint8_t id, uint8_t pin1, uint8_t pin2, void interruptFunc1(void), void interruptFunc2(void));
    void getMove(uint16_t *distance, bool *direction);
    void incrementStep();
    void setDirection();

  private:
    void configureGPIO(void interruptFunc1(void), void interruptFunc2(void));


  private :
    uint16_t mNbStep;
    bool mDirection;
    int mId;
    uint8_t mPin1;
    uint8_t mPin2;

};
