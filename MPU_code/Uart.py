###############################################################################
#  Functions of this code:
#  This code implements the UART protocol defined in the README.md od this folder
###############################################################################

import serial
import time

class Uart():
    #===========================================================================
    ## init function
    #
    # setup uart
    #===========================================================================
    def __init__(self, i_port):
        self._ser = serial.Serial(port= i_port, baudrate = 115200 ,timeout= 0.5)
    #===========================================================================
    ## send_uart_without_response function
    #
    # only send command
    #=========================================================================== 
    def send_uart_without_response (self, trame):
        #Send the UART trame to the MCU
        
        chk =self._calculCHK(b'\x02'+ trame)
        self._ser.write(b'\x02' + trame + bytes([chk]) + b'\x03')
    #===========================================================================
    ## send_uart_with_response function
    #
    # send command and listen response with error detection
    #===========================================================================   
    def send_uart_with_response(self, trame,response_len):
        #Send the UART trame and read response from the MCU
        
        chk =self._calculCHK(b'\x02'+ trame)
        self._ser.write(b'\x02' + trame + bytes([chk]) + b'\x03')
        
        timeout = 0
        t= time.time()
        while((self._ser.read(1) != b'\x02') and (timeout < 0.5)):
            timeout = time.time() - t
            
        if (timeout >500):
            #timeout no response 
            return "Error : Timeout no response !"
        response = self._ser.read(response_len+2)
        
        if (len(response) != response_len+2):
            #incomplete response
            return "Error : inconplete response !"
        if (response[response_len+1] != 0x03):
            #Bad response
            return "Error : Timeout Bad response !"
        
        if (self._verifCHK(b'\x02' + response)):
            if (response[0] == 'e'):
                if response[8] == 0x00:
                    return ("Error 0x00 : Bad generated CHK !")
                elif response[8] == 0x01:
                    return("Error 0x01 : Bad generated command !")
                elif response[8] == 0x02:
                    return("Error 0x02 : timeout when recept command on MCU !")
                else:
                    return("Unknown error")
            else : #OK :)
                return response [:len(response)-2]
        else:
            #error badCHK
            return "Error : Bad CHK recept !"

    #===========================================================================
    ## Calcul CHK function
    #
    # Calculate CHK for serial transmission
    #===========================================================================
    def _calculCHK(self, trame):   
        chk=0
        for byte in trame:
            chk +=byte  
        return ((chk+3)%256)

    #===========================================================================    
    ## Verif CHK function
    #
    # Check CHK after serial reception
    #===========================================================================
    def _verifCHK(self,trame):
        chk=0
        for byte in trame[:len(trame)-2]:
            chk += byte
        if ((chk+3)%256 == trame[len(trame)-2]):
            return 1 #CHK OK
        else:
            return 0 #CHK NOK
        
    #===========================================================================
    ## Close function
    #
    # Close serial port and stop communication with Escuino
    #===========================================================================
    def close(self):
        self._ser.close()
        