#####################################################################################################
# !/usr/bin/python
# -*- coding: ascii -*-
#
#  Functions of this code:
#  This code allows the Raspberry Pi 3 to control all the peripherials which are connected to the MCU:
#
#  Bouton1: Allow the Robot to advance and turn on the LED1
#  Bouton2: Allow the Robot to move back
#  Bouton3: Allow the Robot to turn right and turn on the LED2
#  Bouton4: Allow the Robot to turn left and turn on the LED3
#
#####################################################################################################

import Uart
import time
from threading import Thread
from gpiozero import Button
from gpiozero import LED
import os

button = 0x60
led = 0x40
encodeur = 0x20
moteur = 0x00

right = 0x00
left = 0x02
both =  0x04
disable = 0x06


forward = 0x01
backward = 0x00

if __name__ == '__main__':

    class MainRobot(Thread):

        def __init__(self):
            Thread.__init__(self)

            self.mySerial = Uart.Uart('/dev/ttyS0')   # Instance of the Serial Port
            self.B_states = [0] * 4                   # List which contains the states of the buttons (0:down / 1:up)
            self.L_states = [0] * 3                   # List which contains the states of the LEDs (on/off)
            self.Q_states = [0] * 2                         # List which contains the quadrature encoder outputs

            self.button = Button(23)
            self.led = LED(25)

            self.stop = False
            
        def run(self):
                while(self.stop != True):

                    ## Buttons's states backup
                    for i in range(0, 4):
                        trame_button = button + (i+1)*2
                        self.B_states[i] = self.mySerial.send_uart_with_response(bytes([trame_button]), 1)

                    ## QuadEncoder's states backup
                    trame_quadEncoder = encodeur + 0x04
                    result_encoder = self.mySerial.send_uart_with_response(bytes([trame_quadEncoder]), 5)
                    print(self.Q_states)
                    #Q_state
                    

                    ## Command which allows the robot advances with a speed of 255
                    if self.B_states[0] == b'0':
                        trame_motor = moteur + both + forward
                        self.mySerial.send_uart_without_response(bytes([trame_motor]) + b'\xFF')

                    ## Command which allows the robot to move back with a speed of 255
                    elif self.B_states[1] == b'0':
                        trame_motor = moteur + both + backward
                        trame_led = led + (3*2) + 1
                        self.mySerial.send_uart_without_response(bytes([trame_motor]) + b'\xFF')
                        self.mySerial.send_uart_without_response(bytes([trame_led]))

                    ## Command which allows the robot to turn right
                    elif self.B_states[2] == b'0':
                        trame_motor_1 = moteur + left + forward
                        trame_motor_2 = moteur + right + backward
                        trame_led = led + (1*2) + 1
                        self.mySerial.send_uart_without_response(bytes([trame_motor_1]) + b'\x60')
                        self.mySerial.send_uart_without_response(bytes([trame_motor_2]) + b'\x60')
                        self.mySerial.send_uart_without_response(bytes([trame_led]))
                        print("The robot turns right")

                    ## Command which allows the robot to turn left
                    elif self.B_states[3] == b'0':
                        trame_motor_1 = moteur + left + backward
                        trame_motor_2 = moteur + right + forward
                        trame_led = led + (2*2) + 1
                        self.mySerial.send_uart_without_response(bytes([trame_motor_1]) + b'\x60')
                        self.mySerial.send_uart_without_response(bytes([trame_motor_2]) + b'\x60')
                        self.mySerial.send_uart_without_response(bytes([trame_led]))
                        print("The robot turns left")
                    else:
                        trame_motor = moteur + both + forward
                        self.mySerial.send_uart_without_response(bytes([trame_motor]) + b'\x00')
                        for i in range(0, 3):
                            trame_led = led + (i+1)*2 + 0
                            self.mySerial.send_uart_without_response(bytes([trame_led]))
                    time.sleep(0.1)

                trame_motor = moteur + disable
                self.mySerial.send_uart_without_response(bytes([trame_motor]))
                for i in range(0, 2):
                    trame_led = led + (i + 1) * 2 + 0
                    self.mySerial.send_uart_without_response(bytes([trame_led]))

        def suicide(self):
            self.stop=True

    thread_1 = MainRobot()

    # Thread launch
    thread_1.start()

    while not thread_1.button.is_pressed :
        thread_1.led.on()
        time.sleep(0.1)
        thread_1.led.off()
        time.sleep(0.1)
        pass
    thread_1.led.on()
    thread_1.suicide()
    thread_1.join()

    os.system('shutdown -h now')
