# MPU_code

Partie logicielle MPU du projet Hirobot mené en collaboration avec la startup Hiventive dans le cadre du projet numérique de 3eme année de la filière SEE de l'enseirb-matmeca.

Cette partie implémente le protocole de communication UART défini ci-après sur la Raspberry Pi 3.
Cette dernière est capable d'envoyer des commandes au MCU (STM32F4) permettant de contrôlé tout les périphériques qui y sont connectés et d'interpréter les réponses du MCU en retour.

## Protocole de communication UART

### Début de trame - 1 octet
- 0x02

### Commande principale - 1 octet
- M pour Moteur
- Q pour Encodeur Quadratique
- L pour Led
- B pour Bouton

### Commande secondaire - 1 octet
- Si M ou Q en commande principale -> R pour droit (Right)/L pour gauche (Left)/B pour les deux  (Both)/D pour éteindre (Disable)
- Si L ou B en commande principale -> ID pour identifiant

### Data1 - 1 octet
- Si M en commande principale -> vitesse -> 0 à 255
- Si Q en commande principale -> retour déplacement -> 0x63
- Si L en commande principale -> L allumée (ON/OFF) -> booléen
- Si B en commande principale -> B appuyée (O/N) -> booléen

### Data2 - 1 octet
- Si M en commande principale -> F pour avant (front)/B pour arrière (behind)

### CRC
- Somme de tous les octets modulo 255

### Fin de trame -1 octet
- 0x03

## Exemples de trames
- 0x02 M R 0xFF F CRC 0x03 : Demande au moteur droit d'avancer à une vitesse de 255
- 0x02 M B 0XAA F CRC 0x03 : Demande aux deux moteurs d'avancer à une vitesse de 170
- 0x02 L ID 0x01 0x00 CRC 0x03 : Demande à la led de s'allumer
- 0x02 Q L 0x63 CRC 0x03 : Demande à l'encodeur quadratique gauche le retour du déplacement du moteur gauche
