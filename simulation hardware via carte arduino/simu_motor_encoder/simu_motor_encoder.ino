
int pin1, pin2, pin3, pin4;
int state = 3;
int t, t2;
void setup() {
  Serial.begin(115200);
  //encoder 1 et 2
  pinMode(6, INPUT);
  pinMode(7, INPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(2, LOW);
  digitalWrite(3, LOW);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  t = millis();

  // motor 1 et 2
  pinMode(8, INPUT);
  pinMode(9, INPUT);
  pinMode(10, INPUT);
  pinMode(11, INPUT);
  pinMode(12, INPUT);
  pinMode(13, INPUT);

}

void loop() {

  encoderDir();
  encoder();
  encoder2Dir();
  encoder2();

  motor();
  motor2();
  delay(500);
}

void encoderDir() {
  if (digitalRead(6)) {
    pin1 = 2;
    pin2 = 3;
  } else {
    pin1 = 3;
    pin2 = 2;
  }
}
void encoder() {
  if (millis() - t > analogRead(8) / 4)
    state++;
  if (state >= 4)
    state = 0;
  switch (state) {
    case 0 :
      digitalWrite(pin1, HIGH);
    case 1 :
      digitalWrite(pin2, HIGH);
    case 2 :
      digitalWrite(pin1, LOW);
    case 3 :
      digitalWrite(pin2, LOW);
  }
}

void encoder2Dir() {
  if (digitalRead(7)) {
    pin3 = 4;
    pin4 = 5;
  } else {
    pin3 = 5;
    pin4 = 4;
  }
}
void encoder2() {
  if (millis() - t2 > analogRead(8) / 4)
    state++;
  if (state >= 4)
    state = 0;
  switch (state) {
    case 0 :
      digitalWrite(pin3, HIGH);
    case 1 :
      digitalWrite(pin4, HIGH);
    case 2 :
      digitalWrite(pin3, LOW);
    case 3 :
      digitalWrite(pin4, LOW);
  }
}

void motor() {
  Serial.println("--------------------motor1--------------------");

  Serial.print("enable = ");
  Serial.println(digitalRead(8));

  Serial.print("direction = ");
  if (digitalRead(9))
    Serial.println("FORWARD");
  else
    Serial.println("BACKWARD");

  Serial.print("PWM = ");
  double h = pulseIn(10, HIGH);
  double b = pulseIn(10, LOW);
  Serial.print((h / (h + b)) * 100);
  Serial.println(" %");
}

void motor2() {
  Serial.println("--------------------motor2--------------------");

  Serial.print("enable = ");
  Serial.println(digitalRead(11));

  Serial.print("direction = ");
  if (digitalRead(12))
    Serial.println("FORWARD");
  else
    Serial.println("BACKWARD");

  Serial.print("PWM = ");
  double h = pulseIn(13, HIGH);
  double b = pulseIn(13, LOW);
  Serial.print((h / (h + b)) * 100);
  Serial.println(" %");
}
